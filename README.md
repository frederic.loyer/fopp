# Fundamental of Piano Practice

This is the source file of the LaTeX version of Fundamental of Piano Practice, english and french version.

You can upload it with `git clone https://gitlab.com/frederic.loyer/fopp`

It is compiled with `latexmk -pdf`.
